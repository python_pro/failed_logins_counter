from failed_logins_counter import failed_logins_counter


def test_count_failed_logins_no_entries(mocker):
    mocker.patch('failed_logins_counter.failed_logins_counter.get_log_records_last_hour', return_value=[])
    assert failed_logins_counter.count_failed_logins(failed_logins_counter.get_log_records_last_hour()) == 0


def test_count_failed_logins_few_entries(mocker):
    mock_data = [{'MESSAGE': 'some text pam_unix(sshd:auth): authentication failure; aaa'},
                 {'MESSAGE': 'some text pam_unix(sshd:auth): authentication failure; bbb'},
                 {'MESSAGE': 'some text pam_unix(sshd:auth): authentication failure; ccc'}]
    mocker.patch('failed_logins_counter.failed_logins_counter.get_log_records_last_hour', return_value=mock_data)
    assert failed_logins_counter.count_failed_logins(failed_logins_counter.get_log_records_last_hour()) == 3


def test_count_failed_logins_irrelevant_entries(mocker):
    mock_data = [{'MESSAGE': 'some text'},
                 {'MESSAGE': 'some text'},
                 {'MESSAGE': 'some text'}]
    mocker.patch('failed_logins_counter.failed_logins_counter.get_log_records_last_hour', return_value=mock_data)
    assert failed_logins_counter.count_failed_logins(failed_logins_counter.get_log_records_last_hour()) == 0
