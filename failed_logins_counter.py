from time import time as unix_timestamp

from systemd import journal


def get_log_records_last_hour():
    """
    Filter log records as the following command does:
    $ journalctl -l SYSLOG_FACILITY=10 --priority=5 --since "1 hour ago" --no-pager

    Currently supports only sshd authentication-related records because
     collecting all possible message patterns for different authentication means
     is out of the scope of this task.
    """
    journal_reader = journal.Reader()
    journal_reader.add_match(SYSLOG_FACILITY=10)  # SYSLOG_FACILITY=10
    journal_reader.log_level(journal.LOG_NOTICE)  # --priority=5
    seconds_in_hour = 3600
    journal_reader.seek_realtime(unix_timestamp() - seconds_in_hour)  # --since "1 hour ago"
    journal_reader.add_match(_SYSTEMD_UNIT='ssh.service')  # pick only entries by sshd systemd unit
    return journal_reader


def count_failed_logins(reader):
    """
    Count failed logins in provided journal reader iterator output.

    Currently covers only ssh authentication failures because collecting all
     possible message patterns for different authentication means is out of
     the scope of this task.
    """
    message_pattern = 'pam_unix(sshd:auth): authentication failure'
    return sum([1 for entry in reader if message_pattern in entry['MESSAGE']])


if __name__ == '__main__':
    print('Number of failed logins in the last hour:', count_failed_logins(get_log_records_last_hour()))
